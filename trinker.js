module.exports = {
    title: function (){
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
$$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
$$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
$$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
$$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
$$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
\\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
`.yellow
    },

    line: function(title = "="){
        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black
        
    },

    // allMale: function(p,d){
        //     // var genreMale = p.filter(word => word.gender == "Male");
    //     // return genreMale;
    
    // },

    // allFemale: function(p){
    //     var genreFemale = p.filter(word => word.gender == "Female");
    //     return genreFemale;
    // },
    
    // GetIncome: function(i){
    //    return parseFloat(i.income.slice(1));
    // },
    
    // nbFilm: function(p,d ){
    //     return p.filter(i=>i.pref_movie.includes(d));
    // },
    nbOfMale: function(p,d){
        return this.byGenre(p,d).length;        // je prends la valeur p et d et je veux le nombre de personne qui sont hommes donc d (.length)
                                                // p est mon tableau people
                                        // d est ( nombre de personne qui sont des femmes) ex=> "Male"

        // 
    },

    nbOfFemale: function(p,d){
        return this.byGenre(p,d).length;         // je prends la valeur p et d et je veux le nombre de personne qui sont femmes donc d (.length)
                                                // p est mon tableau people
                                        // d est ( nombre de personne qui sont des femmes) ex=> "Female"
    },

    nbOfMaleInterest: function(p,d){
        return this.byInterest(p,d).length;   // je prends la valeur p et d et je veux le nombre de personne qui son intérréssé par des hommes donc d (.length)
                                                // p est mon tableau people
                                        // d est ( est intérréssé par des hommes ) ex=> "H"
    },

    nbOfFemaleInterest: function(p,d){
        return this.byInterest(p,d).length; // je prends la valeur p et d et je veux le nombre de personne qui son intérréssé par des femmes donc d (.length)
                                                // p est mon tableau people
                                        // d est ( est intérréssé par des femmes) ex=> "F"
        
    },
    
    nbIncomeSup: function(p,n){
       return this.byIncome(p, n).length; // je prends la valeur p et d et je veux le nombre de personne qui gagne d (.length)
                                            // p est mon tableau people
                                    // d est ( montant du salaire) ex=> 2000
        
    },

    nbDrama: function(p,d){
        return this.byMovie(p,d).length; // je prends la valeur p et d et je veux le nombre de personne qui regarde d (.length)
                                        // p est mon tableau people
                                        // d est "Catégorie du film ex : Drama"
        },

    nbFemaleSF: function(p,n,d){
            return this.byMovie(this.byGenre(p, n),d).length; // je prends la valeur p et d et n et je veux le nombre de femme qui regardent des sf (.length)
                                                                // p est mon tableau people
                                                                // d est "Catégorie du film ex : "Sci-Fi"
                                                                // n est "Female"
        },

    // LEVEL 2 //

    nbSupDoc: function(p,d,n){
        return this.byIncome(this.byMovie(p,n),d).length;   // je prends la valeur p et d et n et je veux la taille de toutes les personnes qui gagnent d et qui regardent des documentaires(.length)
                                                            // p est mon tableau people
                                                            // d est "le salaire "
                                                            // n est "type de film" 
    },

    idNNISup: function(p,n){
        return this.byIncome(this.byAll(p),n); 
    },

    richMale: function(p){
        nombres = []
            for ( let x of p){
                // p.filter(x=>x.gender.includes(d));
                salaire= parseFloat((x.income).slice(1))
                nombres.push([salaire, x.id, x.last_name])
            }
        tri = nombres.sort(function(a,b){
            return a[0]-b[0];
        });
        return tri[tri.length-1]
    },

    salMoy: function(p){
       let acc = 0                              // crée une variable Acc qui est égal à 0
        for (let x of p){                       // crée une boucle for qui à pour condition x of p
        acc = acc + this.byIncome_(x)           // acc ( variable) + fonction byIncome_ x
       }                                        // ferme la boucle for 
       return acc / p.length                     // Retourne le résultat acc diviser par la longueur de p
    },                                          // Fin de la fonction

    
    salMed: function(p){
        // il faut que tu tries le tableau p que tu prennes le 499 et le 500 et que tu divises par deux. 
        nombres = []                                        // Variable nombres qui est un tableau vide
        for ( let x of p){                                  // boucle for qui a comme condition x of p
            // p.filter(x=>x.gender.includes(d));           // faut pas lire ça
            salaire= parseFloat((x.income).slice(1))        // variable salaiire qui transforme dans income (people) les chaines de caractères en nombre et qui supprime la première valeur donc $
            nombres.push(salaire)                           // fonction push qui push dans le tableau nombre la variable salaire
        }                                                   // fin de la boucle for
        nombres.sort(function(a,b){                         // dans mon tableau t je trie avec la fonction sort les distances de a (personne 1) - la distance de la b (personne 2)
             return a-b;                                    // je retoune a-b
        })                                                  // fin de la fonction
        return (nombres[499]+nombres[500])/2                // retourne la valeur 499 de mon tableau nombres + la valeur de 500 de mon tableau diviser par deux
    },                                                      // fin de la fonction
    //--------------------------------
        // return lesnbdumid
        // nombres= []
        // for ( let i of p){
        //     return this.byIncome(i)
        // }
        // tri = nombres.sort(function(a,b){
        //     return a - b 
        // })
        // let middleIndex = p.length / 2;
        // if (p.length % 2 !== 0) {
        //     return p[Math.floor(middleIndex)];
        // if(p.length%2 === 0){
        //     let middleIndex = Math.floor(p.length/2)
        //     med = p[middleIndex]
        // }
        //     else{
        //         let middleIndex = Math.floor(p.length/2)
        //         med=p[middleIndex] + p[middleIndex-1]
        //     }
        // }
    //--------------------------------

    moyIncome: function(p){
        let Acc=0                       // crée une variable Acc qui est égal à 0
        for (let I of p){               // crée une boucle for qui à pour condition i of p
        Acc = Acc+this.transformeIncome(I)  // acc ( variable) + fonction transformeIncome i
        }                                   // ferme la boucle for 
        return Acc/p.length;                // Retourne le résultat acc diviser par la longueur de p
      },                                    // Fin de la fonction

      
      
    salSouth: function(d,p){
        return this.salMoy(this.lattitude(d,p)); // retourne la fonction salMoy et la fonction lattiude (d = personne vivants dans le nord, p= tableau people)
    },
    
    nbNorth: function(p,d){
        return this.lattitude(p,d).length;      // retourne la taille de ma fonction lattiude( p = tableau people, d = les personnes vivants dans le nord)
    },

    habBene: function(people, cible){
        
        let t = []                                  // je déclare une variable t qui est un tableau vide
        for(let p of people){                       // boucle  for qui a comme condition p of people
            let d = this.byClose(p, cible)          // déclare variable d qui utilise ma fonction byClose qui a comme condition p, cible (p = tableau people, cible =nom de la personne ciblé)
            t.push({id: p.id,name: p.last_name, distance: d})   // fonction push qui à des objets (id:, name:, distance: = des clés / propriétés) dans t
        }                                                       // je ferme ma boucle
        t = t.sort((a,b)=> a.distance - b.distance)             // dans mon tableau t je trie les distances de a (personne 1) - la distance de la b (personne 2)
        return t                                                // je retoune t
   },      
         // --------------------
        // personne qui habite le plus près de Bérénice il faut dont utiliser math sqrt aka pythagore
        //     r = []
        //     t = []
        //     i = []
        //         {
        //         var béré = p.filter(word=>word.last_name == "Cawt")             // recherche dans le tableau le nom de famille de bérénice
        //         r.push(this.bySearchlola(béré))                                  
        //         }
        //     {
        //         var otherpeople = p.filter(word=>word.p)
        //         t.push(this.bySearchlola(otherpeople))
        //     }
        //  {     
        //     let a = r.lattitude - t.lattitude
        //     let b = r.longitude - t.longitude
        //     let d = Math.sqrt(Math.pow(a,2) + Math.pow(b,2))
        //     i.push(d.id)
        // }
        // {
        // }
        // return i;
        // --------------------

    
    nbSortie: function(p, nb){
        let t = []          //tu déclares un tableau 
        for(x = 1; x <= nb; x++ ){ // ensuite tu fais initialisation du x /une condition / comment ça augmente
            t.push(p[x])  // je renvoie dans t
        }
        return t          // je retourne t
    },
    
    peopleGoogle(p,d){
        return this.byNameid(this.byGoogle(p,d))                           // je prends la valeur p et d 
                                                                        // p est mon tableau people
                                                                        // d est "Google"
    },

    datebth: function(p){
        let d = []                                                        // je crée une variable d qui est un tableau d=[] tableau d
        today = new Date() // date de maintenant                           // je crée une variable today qui a une fonction date ( date d'aujourd'hui)
         for(let x of p){                                                   // je crée une boucle for qui a comme condition x of p
         let birthday = new Date(x.date_of_birth)                           // j'ouvre ma boucle et je lui dit tu créais une variable birthday ou dedans tu mets la fonction new date ou dedans il récupère la date d'anniversaire de x dans le tableau p (people)
         let age = (today-birthday)/ 31557600000                            // je déclare une variable age qui donne le résultat ensuite la (variable today -  variable birthday) diviser par le nombre de seconde dans une année
         d.push([age, x.id, x.first_name])                                  // je fais une fonction push qui push un tableau ou dedans il y a la variable age avec l'id de x et le nom de x dans d
        }                                                                   // je ferme ma boucle for
        d.sort(function(a,b){                                               // je trie avec la fonction sort mon tableau d
           return b[0]-a[0]                                                 // je retourne la première donner de mon tableau 
        })                                                                  // je ferme
        return d                                                            // je retourne d
        },                                                                  // fin de la fonction
         
    // ----------------------- NE PAS REGARDER
    //     let d = []
    //     today = new Date() // date de maintenant
    //      for(let x of p){
    //      let birthday = new Date(x.date_of_birth)
    //      let age = new Date(birthday)
    //     d.push([Math.abs(age.getUTCFullYear() - 2023),x.id, x.last_name, x.first_name])// le truc avec pleons de nombre c'est une année en seconde
    //      }
    //  d.sort(function(a,b){
    //     return b[0]-a[0]
    //  })
    //  return d
    // },
    //---------------------- NE PAS REGARDER
    //<script type="text/javascript">
//     function getAge(date) { 
//         var diff = Date.now() - date.getTime();
//         var age = new Date(diff); 
//         return Math.abs(age.getUTCFullYear() - 1970);
//     }
//     alert(getAge(new Date(1995, 12, 6))); //Date(année, mois, jour)   
// </script>
//----------------------------- NE PAS REGARDER
    
    // FUNCTION FILTRE // 
    

    byClose: function(p1, p2){
        let a = p1.latitude - p2.latitude
        let b = p1.longitude - p2.longitude
        let d = Math.sqrt(Math.pow(a,2) + Math.pow(b,2))
        return d

        // Formule de pythagore 
        // déclare une variable a  qui calcule  la latitude de la personne 1 (p1) moins la latitude de la deuxième personne (p2)
        // déclare une variable b qui calcule la longitude de la personne 1 (p1) moins la longitude de la personne 2 (p2)
        // déclare une variable d ou dedans on utilise la fonction de la racine carré ( math.sqrt) ou dedans on va utiliser la fonction puissance (Math.pow) en rappellant la variable a et ensuite une nouvelle fonction puissance ou on rappelle cette fois ci b
        // et ensuite je retourne d 
    },
    bySearchlola: function(p){
        r = []
        for(let x of p){
            let xsimple = {
            latitude : x.latitude,
            longitude: x.longitude,
        }
        r.push(xsimple)
        }
        return r;

        // je créé un tableau r
        // je fais ensuite une boucle (for) où j'ai comme condition x of p (x pour une personne ) et p (pour le tableau people)
        // j'ouvre la boucle je crée une variable xsimple ou dedans je lui dis dans la propriété latitude tu vas chercher la latitude de x
        // et dans la propriété longitude tu vas chercher la longitude de x
        // et j'utilise la fonction push pour envoyer x simple dans r
        // je ferme ma boucle 
        // et ensuite je retourne mon tableau r 
    }, 

    byNameid: function(p){
        r=[]
        for(let x of p){
            let xsimple={
                id : x.id,
                lastname : x.last_name,
            }
        r.push(xsimple)
        }
        return r;

        // je crée un tableau r 
        // je fais ensuite une boucle (for) où j'ai comme condition x of p (x pour une personne) et p ( pour le tableau people)
        // j'ouvre la boucle je crée une variable xsimple ou dedans je lui dis dans la propriété id tu vas chercher l'id de x 
        // et dans la propriété lastname tu vas chercher le last name de x
        // et jj'utilise la fonction push pour envoyer x simple dans r
        // je ferme ma boucle
        // et ensuite je retourne mon tableau r
    },

    lattitude: function(p,d){
        if(d == "North"){
            return p.filter(i=>i.latitude  > 0)
        }
        if(d == "South"){
            return p.filter(i=>i.latitude < 0 )
        }
        else{
            return p.filter(i=>i.latitute = 0)
        }

        // dans ma fonction lattitude 
        // je mets la condition if qui dit que si d est égal à Nord alors elle retourne toutes les personnes qui sont dans l'émisphère nord
        // return = retourne, p = people, filtre = function (i => i.latitude > 0) si les personnes x ayabt une latitude 0 alors ( je filtre)
        // ensuite si d est égal à Sud alors elle retourne les personnes qui sont dans l'émisphère sud
        // sinon elle retourne les personnes vivant sur l'équateur ( 0)
    },

     longitude: function(p,d){
         if(d == "West"){
             return p.filter(i=>i.longitude > 0)
         }
         if(d == "East"){
             return p.filter(i=>i.longitude < 0)
         }
         else{
             return p.filter(i=>i.longitude = 0)
            }
            // pareil que pour la fonction lattitude sauf que cette fois si c'est avec la longitude
     },
     
    byGenre: function(p,d){
        return p.filter(i=>i.gender.includes(d));
        // je filtre les personnes dans le tableau  les genres des personnes qui inclus d 
        // ex si une personne est une Femme alors ma fonction va chercher dans le tableau p le genre et va seulement récupérer les femmes.
    },
    byGoogle: function(p,d){
        return p.filter(i=>i.email.includes(d));
        // ma fonction filtre les emails des gens dans le tableau (p(people)) qui inclus d ( quand je dis inclus c'est quand on ecrit par exemple ("google") dans algo.js)
    },
    
    byInterest: function(p,d){
        return p.filter(i=>i.looking_for.includes(d));
        // ma fonction filtre les préférences des gens dans le tableau (p (people)) et sort les préférences que nous selectionnons avec d
        // ex : je selectionne juste les personnes qui préfèrent les hommes 
        // ça me retournera la liste des personnes qui sont intéréssées par des hommes.
    },
    
    byMovie: function(p,d){
        return p.filter(i=>i.pref_movie.includes(d));
        // ma fonction filtre les styles de films préférés des gens dans le tableau (p (people)) et sort  les préférences que nous selectionnons avec d
        // ex : je selectionne juste les personne qui aiment les dramas(d )
        // ça me retournera la liste des personnes qui aiment les dramas
    },
    

    byIncome: function(i,n){
        return i.filter(k => this.byIncome_(k) > n);
        // ma fonction filtre les salaires dans le tableau (p, (people)) en fonction de n et là notamment c'est les salaires supérieurs à n 
        
    },
    byIncome_: function(i){
        return parseFloat(i.income.slice(1));
        // cette fonction et en rapport avec ma fonction précédent c'est pour transformer une chaine de caractère (string)
        // en nombre, c'est à dire que sur cette fonction notamment je l'utilise pour enlever le $ dans "$2000"
    },
    byIncome3: function(i){
        return i.filter(k => this.transformeIncome(k) > n);
    },                                                              // CEST LES DEUX MEMES QUE CELLE DU DESSUS JUSTE
    transformeIncome: function(i){                                  // LES DEUX NOMS DES FONCTIONS QUI CHANGENT
        return parseFloat(i.income.slice(1));
    },
    byAll: function(p){
        r =[];
        for(let x of p){
            let xsimple={
                id : x.id,
                firstname: x.first_name + x.last_name,
                income: x.income
            }
            r.push(xsimple)
        }
        return r;
        // je crée un tableau r 
        // je crée ensuite une boucle for ou je lui dit comme condition x of p
        // j'ouvre la boucle 
        // je crée une variable xsimple ou dedans il y a des objets
        // dans la propriété id tu selectionnes l'id de x (.id qui se trouve dans le tableau people)
        // dans la propriété firstname tu selectionnes le nom et le prénom de x
        // dans la propriété income tu selectionnes le salaire de x
        // je sort de la variable xsimple 
        // je push ensuite celle ci dans mon tableau r
        // je sors de ma boucle 
        // et je retourne r 
    },

   //-----------------------
    // byCompare: function(p){
    //    p = []
    //     acc = 0
    //    for (let x of p ){
    //     acc = acc + x.income
    //    }
    //    return moyenne = acc / p.length
        
    // },
    
    // bySort: function(p){
    //     r = []
    //     p.sort((a,b)=> a.income - b.income);
    //     r=p
    //     return  r[r.length-1];
    // },
    

//     byMoney: function(p,d){
//         var d = p[0];
//         for (i=0; i>p.length; i++)
//          {
//              if(p[i] > d)
//              {
//              return d = p[i];
//              }
//          }
//    },
    //-----------------------
    
    match: function(p){
        return "not implemented".red;
    },
}
